Scarborough--Rouge Park Riding Lookup Tool
===========================================

This is the git repository deployed to OpenShift: http://srp-logeo.rhcloud.com 

Requirements:
* Background: Scarborough--Rouge Park is a new federal riding that is being created out of the constituent ridings of Scarborough--Guildwood, Scarborough--Rouge River and Pickering--Scarborough East

* At a high level: Assist Potential members must be able to identify whether or not they are within the new riding of Scarborough--Rouge Park

* For members who are in the new SRP riding, they need to know what their current riding is so they may fill out the appropriate membership form
* For members who are part of one of the three constituent ridings of Scarborough--Rouge Park, they need to know the name of the constituent riding and be told that although they won't fall within the new SRP boundaries, they may still order forms for their constituent association in order to sign other members up


Django Project Details
----------------------

* The main django project files are located in wsgi/openshift

* The main view file is located in wsgi/openshift/apps/geodata/views.py
* The main template file is located in wsgi/openshift/apps/geodata/templates/geodata/postal_ajax.html


Technology Stack
-----------------
* Python/Django
* PostgreSQL/PostGIS
* JavaScript + Leaflet, jQuery
* GEOS, GDAL


Sample Test Cases
-----------------

* M1C 0B2 - Within the new SRP boundaries and part of the old riding of Pickering--Scarborough East
* M1E 5K6 - Outside of the new SRP boundaries but part of the constituent riding of Scarborough--Guildwood
* M4J 2M1 - Neither inside the new SRP boundaries nor part of any of the riding's old constituent ridings   


Data Files & External Libraries
-------------------------------

Four particular shapefiles were used to produce this app. As the terms of service do not allow the files to be retransmitted, please refer to the following websites:
1. Toronto address points: http://www1.toronto.ca/wps/portal/contentonly?vgnextoid=1a66e03bb8d1e310VgnVCM10000071d60f89RCRD
2. Federal electoral district (i.e. riding) boundaries: http://data.gc.ca/eng
3. Federal polling division boundaries: http://data.gc.ca/eng
4. Federal postal code geopoints (unfortunately, not publicly available)
5. Federal forward sortation area (unfortunately, not publicly available)

GIS libraries (C/C++ libraries):
1. Proj4 : http://trac.osgeo.org/proj/
2. GEOS : http://trac.osgeo.org/geos/
3. GDAL : http://www.gdal.org

Front End Libraries:
1. Twitter Bootstrap: http://getbootstrap.com
2. Leaflet: http://leafletjs.com


Load Scripts
------------

To save yourself time and hassle, consider using the load scripts I've written:

1.  .openshift / bootstrap.sh: 
* Downloads and installs external GIS libraries
* Downloads and truncates publicly-available shapefiles

2. / wsgi / openshift / apps / geodata / scripts / load.py

Addmitedly not the most efficient way of doing this. I was under a severe time crunch when we had to throw it together. Will clean it up soon. 

* Loads data from the shapefiles into the database
* Calculates foreign keys and other associations


Feedback
--------

Always looking to learn, grow and share my experiences. Drop me a line at abmohan [at] gmail.com.