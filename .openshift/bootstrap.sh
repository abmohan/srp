#!/bin/bash

export CFLAGS="-O3 -s"
export CXXFLAGS="-O3 -s"
export OPT="-O3 -s"
export PATH=$OPENSHIFT_DATA_DIR/bin:$PATH

cd $OPENSHIFT_TMP_DIR
wget http://download.osgeo.org/proj/proj-4.8.0.tar.gz
tar xzf proj-4.8.0.tar.gz
cd proj-4.8.0
./configure --prefix=$OPENSHIFT_DATA_DIR
make
make install
cd ..
rm -rf ./proj-4.8.0*


cd $OPENSHIFT_TMP_DIR
wget http://download.osgeo.org/geos/geos-3.3.8.tar.bz2
tar xjf geos-3.3.8.tar.bz2
cd geos-3.3.8
./configure --prefix=$OPENSHIFT_DATA_DIR
make
make install
cd ..
rm -rf ./geos-3.3.8*


cd $OPENSHIFT_TMP_DIR
wget ftp://ftp.remotesensing.org/gdal/gdal-1.9.2.tar.gz
tar zxf gdal-1.9.2.tar.gz
cd gdal-1.9.2
./configure --prefix=$OPENSHIFT_DATA_DIR --disable-static --with-geos=$OPENSHIFT_DATA_DIR/bin/geos-config
make
make install
cd ..
rm -rf ./gdal-1.9.2*

wget http://opendata.toronto.ca/gcc/address_points_wgs84.zip
unzip address_points_wgs84.zip -d address_points_wgs84
rm address_points_wgs84.zip

wget http://ftp2.cits.rncan.gc.ca/pub/geott/electoral/2011/fed308.2011.zip
unzip fed308.2011.zip -d fed308.2011
rm fed308.2011.zip

wget http://ftp2.cits.rncan.gc.ca/pub/geott/electoral/2011/pd308.2011.zip
unzip pd308.2011.zip -d pd308.2011
rm pd308.2011.zip



ogr2ogr SRPpostalcodes.shp CANmep.shp -sql "select * from CANmep where substr(postalcode,1,3) in ('M1E', 'M1C', 'M1B', 'M1X')"
ogr2ogr SRPfsas.shp CANfsa.shp -sql "select * from CANfsa where fsa in ('M1E', 'M1C', 'M1B', 'M1X')"
ogr2ogr SRPridings.shp FED_CA_1.0_0_ENG.shp -sql "select * from FED_CA_1.0_0_ENG  where fednum in (35072, 35082, 35083)"


ogr2ogr SRPpolls.shp pd_a.shp -sql "select * from pd_a where fed_num in (35072, 35082, 35083)"


 # CREATE LANGUAGE plpgsql;
psql -f /usr/share/pgsql/contrib/postgis-64.sql
psql -f /usr/share/pgsql/contrib/spatial_ref_sys.sql
