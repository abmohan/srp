from django.conf.urls import patterns, include, url

from django.conf.urls.static import static
from django.conf import settings

from apps.geodata import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'apps.geodata.views.postal_ajax'),
    url(r'^geo/', include('apps.geodata.urls')),

#    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
