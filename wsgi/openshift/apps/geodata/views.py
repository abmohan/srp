from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.utils import simplejson
import socket

from django.shortcuts import render_to_response
from django.http import HttpResponse
from apps.geodata.models import *

from django.shortcuts import render
from django.http import HttpResponseRedirect

from django.core.exceptions import ObjectDoesNotExist

from django.contrib.gis.geos import GEOSGeometry
from djgeojson.serializers import Serializer as GeoJSONSerializer

import re


def postal_test(request):

    return render_to_response('geodata/lookup.html', context_instance=RequestContext(request))


def postal(request):

    return render_to_response('geodata/postalcode.html', context_instance=RequestContext(request))


def postal_ajax(request):

    return render_to_response('geodata/postal_ajax.html', context_instance=RequestContext(request))


def postal_json(request):

    if request.POST.has_key('postalcode'):

        # postal code object (trimmed, eliminated of spaces and in upper case)
        q_postalcode = request.POST['postalcode']

        # response dictionary
        response_dict = {}

        try:

            postalcodes = PostalCode.objects.filter(postalcode=q_postalcode)

            # make postal code more readable, for JSON object
            q_postalcode = q_postalcode[:3] + " " + q_postalcode[3:]

            pc_in_riding = sum([1 if pc.in_riding else 0 for pc in postalcodes])
            num_pc = len(postalcodes)

            # case 1: all postal codes are in riding
            if pc_in_riding == num_pc:

                postalcode = postalcodes[0]

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': True,
                        'in_riding': True,
                        'srp_overlap': "Full",

                        'fsa': postalcode.fsa,
                        'riding': postalcode.riding.english_name
                    }
                )

            # case 2: some postal codes are in riding
            elif  0 < pc_in_riding < num_pc:

                postalcode = postalcodes[0]

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': True,
                        'in_riding': True,
                        'srp_overlap': "Partial",

                        'fsa': postalcode.fsa,
                        'riding': postalcode.riding.english_name
                    }
                )

            # case 3: no postal codes are in riding, but they can order forms
            else:

                postalcode = postalcodes[0]

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': True,
                        'in_riding': False,
                        'srp_overlap': "None",

                        'fsa': postalcode.fsa,
                        'riding': postalcode.riding.english_name
                    }
                )

        except:

                postalcode = None

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': False,
                        'in_riding': False,
                        'srp_overlap': "None",
                    }
                )    

        return HttpResponse(simplejson.dumps(response_dict), mimetype='application/javascript')

    else:

        return render_to_response('geodata/postal_ajax.html', context_instance=RequestContext(request))


def geopoint_json(request):

    # response dictionary
    response_dict = {}

    if request.POST.has_key('geopoint'):

        # postal code object (trimmed, eliminated of spaces and in upper case)
        q_geopoint = request.POST['geopoint']

        response_dict['geopoint'] = q_geopoint

        try:

            # load geopoint from json object
            geopoint = GEOSGeometry(q_geopoint)

            # lookup riding intersection
            ridings = FederalRiding.objects.filter(geom__contains=geopoint)

            # check if point is in Scarborough--Rouge Park
            response_dict['in_srp'] = len(ridings.filter(english_name="Scarborough--Rouge Park"))>0

            # check if point includes another riding
            if (ridings.exclude(english_name="Scarborough--Rouge Park")):
                response_dict["current_riding"] = ridings.exclude(english_name="Scarborough--Rouge Park")[0].english_name
            else:
                response_dict["current_riding"] = ""


        except:

            response_dict.update(
                {
                    'geopoint': q_geopoint,

                    'found': False,
                }
            )

        return HttpResponse(simplejson.dumps(response_dict), mimetype='application/javascript')

    else:

        return render_to_response('geodata/postal_ajax.html', context_instance=RequestContext(request))


def postal_geojson(request):

    # response dictionary
    response_dict = {}

    if request.POST.has_key('postalcode'):

        # postal code object (trimmed, eliminated of spaces and in upper case)
        q_postalcode = request.POST['postalcode']

        try:

            postalcodes = PostalCode.objects.filter(postalcode=q_postalcode).values("post_code", "geom")
            response_data = GeoJSONSerializer().serialize(postalcodes, use_natural_keys=True)

            return HttpResponse(response_data, mimetype='application/javascript')

        except:

                postalcode = None

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': False,
                    }
                )

        return HttpResponse(simplejson.dumps(response_dict), mimetype='application/javascript')

    else:
        return render_to_response('geodata/postal_ajax.html', context_instance=RequestContext(request))


def postalcode_details(request, postalcode):

        # response dictionary
        response_dict = {}

        # remove wuitespace from postalcode and convert to upper case
        q_postalcode = re.sub(re.compile(r'\s+'), '', postalcode).upper()


        try:

            postalcodes = PostalCode.objects.filter(postalcode=q_postalcode)

            # make postal code more readable, for JSON object
            q_postalcode = q_postalcode[:3] + " " + q_postalcode[3:]

            postalcode = postalcodes[0]

            response_dict.update(
                {
                    'postalcode': q_postalcode,

                    'found': True,

                    'fsa': postalcode.fsa,
                    'riding': postalcode.riding.english_name,
                    'poll': postalcode.poll.__unicode__(),
                    'poll_num': postalcode.poll.pd_num,
                    'zone': postalcode.poll.zone.number
                }
            )

        except:

            postalcode = postalcodes[0] if postalcodes and postalcodes[0] else None

            response_dict.update(
                {
                    'postalcode': q_postalcode,
                    'fsa': postalcode.fsa if postalcode and postalcode.fsa else None,
                    'riding': postalcode.riding.english_name if postalcode and postalcode.riding and postalcode.riding.english_name else None,
                    'poll': postalcode.poll.__unicode__() if postalcode and postalcode.poll else None,
                    'poll_num': postalcode.poll.pd_num if postalcode and postalcode.poll else None,
                    'found': False
                }
            )

        return HttpResponse(simplejson.dumps(response_dict), mimetype='application/javascript')
