from django.contrib.gis import admin
from django.contrib.admin import TabularInline
from models import FederalPoll, FederalRiding, TorontoAddressPoint
from models import PostalCode, PostalCodeForwardSortationArea
from models import Zone, Region

class FederalRidingAdmin(admin.OSMGeoAdmin):

    list_display = ['english_name', 'province']
    list_filter = ['english_name']
    ordering = ['fednum']

    fieldsets = [
        (None, {
            'fields': ['english_name', 'geom']
            }
        ),
        ('Elections Canada Fields', {
            'fields': ['fednum', 'french_name', 'province', 'nid', 'created_date', 
                'revised_date', 'reporder', 'decpopcnt', 'quipopcnt', 'enlegaldsc', 
                'frlegaldsc', 'shape_area', 'shape_len'], 
            'classes': ['collapse']
            }
        ),
    ]


class FederalPollAdmin(admin.OSMGeoAdmin):

    list_display = ['__unicode__', 'adv_poll', 'in_riding', 'zone']
    list_filter = ['riding', 'in_riding', 'zone']
    #'classes': ['collapse']


class PostalCodeFSAAdmin(admin.OSMGeoAdmin):

    list_filter = ['fsa', 'province']
    ordering = ['fsa', 'province']  

    fieldsets = [
        (None, {
            'fields': ['fsa', 'geom', 'province']
            }
        ),
    ]


class PostalCodeAdmin(admin.OSMGeoAdmin):

    list_filter = ['postalcode', 'province', 'fsa']
    ordering = ['postalcode', 'province', 'fsa']   
    search_fields = ['postalcode', 'post_code']

    fieldsets = [
        (None, {
            'fields': ['post_code', 'in_riding', 'srp_overlap', 'fsa', 'poll', 'riding','geom']
            }
        ),
        ('Canada Post Fields', {
            'fields': ['postalcode', 'mep_id', 'sli', 'province', 'comm_name', 'maf_id', 'birth_date', 'ret_date', 'dom_delmde', 'total_poc', 'poc_apart', 'poc_bus', 'poc_house', 'poc_farm', 'total_cc', 'cc_apart', 'cc_bus', 'cc_house', 'cc_farm', 'pc_count', 'position', 'longitude', 'latitude', 'canmapid'], 
            'classes': ['collapse']
            }
        ),
    ]  



class TorontoAddressPointAdmin(admin.OSMGeoAdmin):

    list_display = ['__unicode__', 'fedriding_englishname', 'fedpoll_number', 'fcode_des', 'name']
    list_filter = ['in_riding', 'srp_overlap', 'federal_riding', 'fcode_des', 'name']
    ordering = ['in_riding', 'srp_overlap', 'federal_riding', 'lfname', 'lonum', 'address']
    search_fields = ['lfname', 'name']

    def fedriding_englishname(self, obj):
        return "%s" %(obj.federal_riding.english_name)
    
    fedriding_englishname.short_description = "Riding"
    fedriding_englishname.admin_order_field = 'federal_riding__english_name'

    def fedpoll_number(self, obj):
        return "%s" %(obj.federal_poll.pd_num)
    
    fedpoll_number.short_description = "Poll"
    fedpoll_number.admin_order_field = 'federal_poll__pd_num'

    fieldsets = [
        (None, {
            'fields': ['address', 'lfname', 'postal_code', 'federal_riding', 'federal_poll', 'ward_name',
                'in_riding', 'srp_overlap', 'arc_side', 'classification', 'name', 'geom']
            }
        ),
        ('City of Toronto Fields', {
            'fields': ['geo_id', 'link', 'lonum', 'lonumsuf', 'hinum', 'hinumsuf', 'distance', 'fcode', 'fcode_des', 'x', 'y', 'longitude', 'latitude', 'objectid', 'municipality_name'], 
            'classes': ['collapse']
            }
        ),
    ]

class ZoneAdmin(admin.OSMGeoAdmin):
    list_display = ['__unicode__', 'admin_image']
    list_filter = ['number', 'region']
    readonly_fields = ('admin_image',)
    exclude = ('geom',)


admin.site.register(FederalRiding, FederalRidingAdmin)
admin.site.register(FederalPoll, FederalPollAdmin)

admin.site.register(TorontoAddressPoint, TorontoAddressPointAdmin)

admin.site.register(PostalCode, PostalCodeAdmin)
admin.site.register(PostalCodeForwardSortationArea, PostalCodeFSAAdmin)

admin.site.register(Zone, ZoneAdmin)
admin.site.register(Region, admin.OSMGeoAdmin)