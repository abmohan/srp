from django.contrib.gis.db import models

class FederalRiding(models.Model):
    nid = models.CharField(max_length=36, verbose_name="National identifier")
    fednum = models.IntegerField(verbose_name="Riding number")

    english_name = models.CharField(max_length=100)
    french_name = models.CharField(max_length=100)
    province = models.CharField(max_length=2)

    created_date = models.CharField(max_length=8)
    revised_date = models.CharField(max_length=8)
    reporder = models.CharField(max_length=8)
    decpopcnt = models.IntegerField()
    quipopcnt = models.IntegerField()
    
    enlegaldsc = models.CharField(max_length=200)
    frlegaldsc = models.CharField(max_length=200)

    shape_area = models.FloatField()
    shape_len = models.FloatField()

    geom = models.MultiPolygonField(srid=4269)
    objects = models.GeoManager()

    def __unicode__(self):
        return "%s (%s)" %(self.english_name, self.fednum)

    class Meta:
        unique_together = ('nid', 'fednum', 'revised_date')


# Auto-generated `LayerMapping` dictionary for FederalRiding model
federalriding_mapping = {
    'nid' : 'NID',
    'fednum' : 'FEDNUM',
    'english_name' : 'ENNAME',
    'french_name' : 'FRNAME',
    'province' : 'PROVCODE',
    'created_date' : 'CREADT',
    'revised_date' : 'REVDT',
    'reporder' : 'REPORDER',
    'decpopcnt' : 'DECPOPCNT',
    'quipopcnt' : 'QUIPOPCNT',
    'enlegaldsc' : 'ENLEGALDSC',
    'frlegaldsc' : 'FRLEGALDSC',
    'shape_area' : 'SHAPE_AREA',
    'shape_len' : 'SHAPE_LEN',
    'geom' : 'MULTIPOLYGON',
}



class FederalPoll(models.Model):

    PD_TYPES = (
        ('S', 'Single Building Poll'),
        ('M', 'Mobile Polls'),
    )

    URBANRURAL_CHOICES = (
        ('U', 'URBAN'),
        ('R', 'RURAL'),
    )

    OVERLAP_CHOICES = (
        ('F', 'Full'),
        ('P', 'Partial'),
        ('N', 'None'),
    )

    pd_id = models.IntegerField(verbose_name="Elections Canada database unique ID for Poll")
    pd_num = models.IntegerField(verbose_name="Poll number")
    pd_nbr_sfx = models.CharField(max_length=6, verbose_name="Poll suffix")

    pd_type = models.CharField(max_length=1, choices=PD_TYPES, verbose_name="Poll Type", null=True, blank=True)

    adv_poll = models.CharField(max_length=3, verbose_name="Advanced Poll Number")

    zone = models.ForeignKey('Zone', null=True, blank=True, verbose_name="Zone Number (Gary's Campaign)")

    ed_id = models.IntegerField(verbose_name="Elections Canada database unique ID for Riding")
    fed_num = models.IntegerField(verbose_name="Riding Number")
    riding = models.ForeignKey('FederalRiding', null=True, blank=True)


    a_updt_dte = models.DateField(verbose_name="Last attribute modification")
    g_updt_dte = models.DateField(verbose_name="Last geometric modification")

    emrp_name = models.CharField(max_length=120)
    poll_name = models.CharField(max_length=100)

    pn_updt_dt = models.DateField(null=True, blank=True, verbose_name="Last poll name modification")
    ad_updt_dt = models.DateField(null=True, blank=True, verbose_name="Last advanced poll district modification")

    urban_rura = models.CharField(max_length=2, choices=URBANRURAL_CHOICES, verbose_name="Urban-Rural indicator")

    shape_leng = models.FloatField()
    shape_area = models.FloatField()

    in_riding = models.NullBooleanField(null=True, blank=True, verbose_name="Poll Overlaps Scarborough--Rouge Park")
    srp_overlap = models.CharField(null=True, blank=True, max_length=1, choices=OVERLAP_CHOICES, verbose_name="Scarborough--Rouge Park Overlap")

    geom = models.MultiPolygonField(srid=4269)

    objects = models.GeoManager()

    def __unicode__(self):

        if self.riding:
            return "Poll %s (%s)" %(self.pd_num, self.riding.english_name)
        else:
            return "Poll %s (riding not loaded)" %(self.pd_num) 

    class Meta:
        unique_together = ('pd_id', 'pd_num', 'pd_nbr_sfx', 'fed_num')

# Auto-generated `LayerMapping` dictionary for FederalPoll model
federalpoll_mapping = {
    'pd_id' : 'PD_ID',
    'pd_num' : 'PD_NUM',
    'pd_nbr_sfx' : 'PD_NBR_SFX',
    'pd_type' : 'PD_TYPE',
    'adv_poll' : 'ADV_POLL',
    'ed_id' : 'ED_ID',
    'fed_num' : 'FED_NUM',
    'a_updt_dte' : 'A_UPDT_DTE',
    'g_updt_dte' : 'G_UPDT_DTE',
    'emrp_name' : 'EMRP_NAME',
    'poll_name' : 'POLL_NAME',
    'pn_updt_dt' : 'PN_UPDT_DT',
    'ad_updt_dt' : 'AD_UPDT_DT',
    'urban_rura' : 'URBAN_RURA',
    'shape_leng' : 'Shape_Leng',
    'shape_area' : 'Shape_Area',
    'geom' : 'MULTIPOLYGON',
}


class TorontoAddressPoint(models.Model):

    ARCSIDE_CHOICES = (
        ('L', "Left"),
        ('R', "Right")
    )

    OVERLAP_CHOICES = (
        ('F', 'Full'),
        ('P', 'Partial'),
        ('N', 'None'),
    )

    geo_id = models.IntegerField(verbose_name="City of Toronto Unique ID")
    link = models.IntegerField(verbose_name="Primary Address Unique ID")

    address = models.CharField(max_length=20, verbose_name="Street Number & Suffix")
    lfname = models.CharField(max_length=110, verbose_name="Street Name")
    lonum = models.IntegerField(verbose_name="Low Number")
    lonumsuf = models.CharField(max_length=3, verbose_name="Low Number Suffix")
    hinum = models.IntegerField(verbose_name="High Number")
    hinumsuf = models.CharField(max_length=3, verbose_name="High Number Suffix")

    arc_side = models.CharField(max_length=1, choices=ARCSIDE_CHOICES, verbose_name="Location with respect to street direction")
    distance = models.FloatField(verbose_name="Distance from start of street segment")

    fcode = models.IntegerField(verbose_name="Feature code number")
    fcode_des = models.CharField(max_length=254, verbose_name="Feature code description")
    classification = models.CharField(max_length=20, verbose_name="Address classification")
    name = models.CharField(max_length=200)
    x = models.FloatField()
    y = models.FloatField()
    longitude = models.FloatField()
    latitude = models.FloatField()
    objectid = models.FloatField()

    municipality_name = models.CharField(max_length=15)
    ward_name = models.CharField(max_length=40)
    federal_poll = models.ForeignKey('FederalPoll', null=True, blank=True)
    federal_riding = models.ForeignKey('FederalRiding', null=True, blank=True)
    postal_code = models.ForeignKey('PostalCode', null=True, blank=True)

    in_riding = models.NullBooleanField(null=True, blank=True, verbose_name="Address in Scarborough--Rouge Park")
    srp_overlap = models.CharField(null=True, blank=True, max_length=1, choices=OVERLAP_CHOICES, verbose_name="Scarborough--Rouge Park Overlap")

    geom = models.MultiPointField(srid=4326)
    objects = models.GeoManager()

    def __unicode__(self):
        return "%s %s" %(self.address, self.lfname)

    class Meta:
        unique_together = ('geo_id', 'link')

# Auto-generated `LayerMapping` dictionary for TorontoAddressPoint model
torontoaddresspoint_mapping = {
    'geo_id' : 'GEO_ID',
    'link' : 'LINK',
    'address' : 'ADDRESS',
    'lfname' : 'LFNAME',
    'lonum' : 'LONUM',
    'lonumsuf' : 'LONUMSUF',
    'hinum' : 'HINUM',
    'hinumsuf' : 'HINUMSUF',
    'arc_side' : 'ARC_SIDE',
    'distance' : 'DISTANCE',
    'fcode' : 'FCODE',
    'fcode_des' : 'FCODE_DES',
    'classification' : 'CLASS',
    'name' : 'NAME',
    'x' : 'X',
    'y' : 'Y',
    'longitude' : 'LONGITUDE',
    'latitude' : 'LATITUDE',
    'objectid' : 'OBJECTID',
    'municipality_name' : 'MUN_NAME',
    'ward_name' : 'WARD_NAME',
    'geom' : 'MULTIPOINT',
}

class PostalCodeForwardSortationArea(models.Model):

    fsa = models.CharField(max_length=3, verbose_name="Forward Sortation Area", unique=True)
    province = models.CharField(max_length=2)

    geom = models.MultiPolygonField(srid=4269)
    objects = models.GeoManager()

    def __unicode__(self):
        return "%s" %(self.fsa)

postalcodefsa_mapping = {
    'fsa' : 'FSA',
    'province' : 'PROV',
    'geom' : 'MULTIPOLYGON',
}


class PostalCode(models.Model):

    OVERLAP_CHOICES = (
        ('F', 'Full'),
        ('P', 'Partial'),
        ('N', 'None'),
    )

    mep_id = models.IntegerField(unique=True)

    postalcode = models.CharField(max_length=6)
    post_code = models.CharField(max_length=7)

    sli = models.IntegerField()
    
    province = models.CharField(max_length=2)
    
    comm_name = models.CharField(max_length=68)
    maf_id = models.IntegerField()
    birth_date = models.CharField(max_length=8)
    ret_date = models.CharField(max_length=8)
    dom_delmde = models.CharField(max_length=2)
    total_poc = models.IntegerField()
    poc_apart = models.IntegerField()
    poc_bus = models.IntegerField()
    poc_house = models.IntegerField()
    poc_farm = models.IntegerField()
    total_cc = models.IntegerField()
    cc_apart = models.IntegerField()
    cc_bus = models.IntegerField()
    cc_house = models.IntegerField()
    cc_farm = models.IntegerField()
    pc_count = models.IntegerField()
    position = models.IntegerField()
    longitude = models.FloatField()
    latitude = models.FloatField()
    canmapid = models.IntegerField()

    fsa = models.ForeignKey('PostalCodeForwardSortationArea', null=True, blank=True)

    poll = models.ForeignKey('FederalPoll', null=True, blank=True)
    riding = models.ForeignKey('FederalRiding', null=True, blank=True)

    in_riding = models.NullBooleanField(null=True, blank=True, verbose_name="Address in Scarborough--Rouge Park")
    srp_overlap = models.CharField(null=True, blank=True, max_length=1, choices=OVERLAP_CHOICES, verbose_name="Scarborough--Rouge Park Overlap")

    geom = models.MultiPointField(srid=4269)
    objects = models.GeoManager()

    def __unicode__(self):
        return "%s" %(self.post_code)

postalcodemep_mapping = {
    'mep_id' : 'MEP_ID',
    'postalcode' : 'POSTALCODE',
    'post_code' : 'POST_CODE',
    'sli' : 'SLI',
    'province' : 'PROV',
    'comm_name' : 'COMM_NAME',
    'maf_id' : 'MAF_ID',
    'birth_date' : 'BIRTH_DATE',
    'ret_date' : 'RET_DATE',
    'dom_delmde' : 'DOM_DELMDE',
    'total_poc' : 'TOTAL_POC',
    'poc_apart' : 'POC_APART',
    'poc_bus' : 'POC_BUS',
    'poc_house' : 'POC_HOUSE',
    'poc_farm' : 'POC_FARM',
    'total_cc' : 'TOTAL_CC',
    'cc_apart' : 'CC_APART',
    'cc_bus' : 'CC_BUS',
    'cc_house' : 'CC_HOUSE',
    'cc_farm' : 'CC_FARM',
    'pc_count' : 'PC_COUNT',
    'position' : 'POSITION',
    'longitude' : 'LONGITUDE',
    'latitude' : 'LATITUDE',
    'canmapid' : 'CANMAPID',
    'geom' : 'MULTIPOINT',
}

class Region(models.Model):
    name = models.CharField(max_length=20, unique=True)
    description = models.TextField(max_length=200, null=True, blank=True)

    geom = models.MultiPolygonField(srid=4269, null=True, blank=True)    

    def __unicode__(self):
        return "Region %s" %(self.name)

class Zone(models.Model):

    number = models.IntegerField(unique=True)
    region = models.ForeignKey('Region', null=True, blank=True)
    description = models.TextField(max_length=200, null=True, blank=True)
    map_jpeg = models.ImageField(upload_to='zonemaps')

    geom = models.MultiPolygonField(srid=4269, null=True, blank=True)

    def admin_image(self):
        return "<a href='%s'><img src='%s' height='100', width='100'/></a>" %(self.map_jpeg.url, self.map_jpeg.url)
    admin_image.short_description = "JPEG Map"
    admin_image.allow_tags = True

    def __unicode__(self):
        return "Zone %s" %(self.number)