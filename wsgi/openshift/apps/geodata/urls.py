from django.conf.urls import patterns, url

from apps.geodata import views

urlpatterns = patterns('',

    # ex: /geo/
#    url(r'^postal/$', 'apps.geodata.views.postal', name='postal'),
#    url(r'^postal/$', 'apps.geodata.views.postal_test', name='postal'),
#    url(r'^postal-json$', 'apps.geodata.views.postal_json', name='postal_json'),

    url(r'^$', 'apps.geodata.views.postal_ajax', name='postal_ajax'),
    url(r'^postal/$', 'apps.geodata.views.postal_ajax', name='postal_ajax'),
    url(r'^postal-ajax/$', 'apps.geodata.views.postal_ajax', name='postal_ajax'),
    url(r'^postal-json/$', 'apps.geodata.views.postal_json', name='postal_json'),
    url(r'^postal-geojson/$', 'apps.geodata.views.postal_geojson', name='postal_geojson'),
    url(r'^geopoint-json/$', 'apps.geodata.views.geopoint_json', name='geopoint_json'),

    url(r'^postalcode/(?P<postalcode>\w+)/$', 'apps.geodata.views.postalcode_details', name='postalcode_details'),

)