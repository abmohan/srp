#!/usr/bin/python

from datetime import date
import os

from django.contrib.gis.utils import LayerMapping
from apps.geodata.scripts import srpgeopoints
from django.contrib.gis import geos

from apps.geodata.models import FederalPoll, federalpoll_mapping, FederalRiding, federalriding_mapping
from apps.geodata.models import TorontoAddressPoint, torontoaddresspoint_mapping
from apps.geodata.models import PostalCodeForwardSortationArea, postalcodefsa_mapping
from apps.geodata.models import PostalCode, postalcodemep_mapping


## Load data from shapefile to FederalRiding table
def load_ridings(verbose=True):

	# set data directory depending on whether we're in the dev or production environment
	if os.environ.has_key('OPENSHIFT_DATA_DIR'):
	    data_directory = os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'geodata')
	else:
	    data_directory= os.path.join(os.environ['ELECTION2015_REPO_DIR'], 'data', 'geodata')

	filename = 'fed308.2011/FED_CA_1.0_0_ENG.shp'
	shapefile = os.path.join(data_directory, filename)

	lm = LayerMapping(FederalRiding, shapefile, federalriding_mapping, transform=True, encoding='utf8')
	lm.save(strict=False, verbose=verbose, progress=True)


## Delete ridings outside of Gary's borders
def prune_ridings():

	print "\n\nPruning riding objects"
	outside_ridings = FederalRiding.objects.exclude(fednum__in=[35072, 35082, 35083])
	outside_ridings.delete()


## Load data from shapefile to FederalPoll table
def load_polls(verbose=True):

	# set data directory depending on whether we're in the dev or production environment
	if os.environ.has_key('OPENSHIFT_DATA_DIR'):
		data_directory = os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'geodata')
	else:
		data_directory= os.path.join(os.environ['ELECTION2015_REPO_DIR'], 'data', 'geodata')

	filename = 'pd308.2011/SRPpolls.shp'
	shapefile = os.path.join(data_directory, filename)

	lm = LayerMapping(FederalPoll, shapefile, federalpoll_mapping, transform=True, encoding='utf8')
	lm.save(strict=True, verbose=verbose, progress=True)


## Delete ridings outside of Gary's borders
def prune_polls():

	print "\n\nPruning poll objects"
	outside_polls = FederalPoll.objects.exclude(fed_num__in=[35072, 35082, 35083])
	outside_polls.delete()


# loop through the FederalPoll objects and assign the appropriate riding foreign keys
def load_pollforeignkeys():

	print "Loading poll foreign keys"
	federal_polls = FederalPoll.objects.all()

    # find the ward with the poll's ward number and save the reference
	for federal_poll in federal_polls:
		federal_poll.riding = FederalRiding.objects.get(fednum=federal_poll.fed_num)
		print "\tLoading poll %s from riding %s" %(federal_poll.pd_num, federal_poll.riding.english_name)
		federal_poll.save()


## load all Toronto address points from shapefile into database
def load_addresspoints(verbose=True):

	# set data directory depending on whether we're in the dev or production environment
	if os.environ.has_key('OPENSHIFT_DATA_DIR'):
		data_directory = os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'geodata')
	else:
		data_directory= os.path.join(os.environ['ELECTION2015_REPO_DIR'], 'data', 'geodata')

	filename = 'address_points_wgs84/SRPaddresspoints.shp'
	shapefile = os.path.join(data_directory, filename)

	lm = LayerMapping(TorontoAddressPoint, shapefile, torontoaddresspoint_mapping, transform=True, encoding='utf8')
	lm.save(strict=False, verbose=verbose, progress=True)


def prune_addresspoints():

	print "\n\nPruning address point objects"
	outside_polls = TorontoAddressPoint.objects.exclude(ward_name__in=["Scarborough-Rouge River (42)", "Scarborough East (43)", "Scarborough East (44)"])
	outside_polls.delete()


## loop through the TorontoAddressPoint objects and assign the appropriate poll foreign keys
def load_addresspointforeignkeys():

	"Loading address point foreign keys"
	toronto_addresses = TorontoAddressPoint.objects.filter(federal_poll__isnull=True)

	# counter variables for loop output
	i = 1
	num_addresses = len(toronto_addresses)

	# keep track of any errors
	errors = []

	# find the poll that the address point is located in
	for toronto_address in toronto_addresses:

		try:

			if not toronto_address.federal_poll:

				toronto_address.federal_poll = FederalPoll.objects.get(geom__contains=toronto_address.geom)
				toronto_address.federal_riding = toronto_address.federal_poll.riding                
				toronto_address.save()

				print "\t%d/%d  Loading %s into %s" %(i, num_addresses, toronto_address, toronto_address.federal_poll, toronto_address.federal_riding)
				i += 1

		# If poll or riding not found
		except:
			errors.append(toronto_address.__unicode__())

	if errors:
		print "The following address points failed to load:"
		print "\n\t".join(errors)


## Load Scarborough--Rouge Park riding boundaries into database
def load_srpgeopoints():

	# import geojson string
	srp_geom = geos.fromstr(srpgeopoints.geojson)

	# create riding by copying an existing riding
	srp = FederalRiding.objects.all()[0]

	# create new model instance
	srp.id = None
	srp.save()

	# insert appropriate values
	srp.geom = srp_geom
	srp.english_name = "Scarborough--Rouge Park"
	srp.french_name = "Scarborough--Rouge Park"

	srp.fednum = 2015
	srp.province = "ON"
	srp.created_date = str(date.today()).replace("-", "")
	srp.revised_date = str(date.today()).replace("-", "")

	# insert standard values for non-nullable fields
	srp.nid = "srp"
	srp.reporder = "srp"
	srp.decpopcnt = -1
	srp.quipopcnt = -1
	srp.enlegaldsc = "srp"
	srp.frlegaldsc = "srp"
	srp.shape_area = -1
	srp.shape_len = -1

	# save to database
	srp.save()

## Set values indicating whether polls are within the riding
def load_srp_polls():

	srp = FederalRiding.objects.filter(english_name="Scarborough--Rouge Park")[0]

	polls = FederalPoll.objects.all()

	for poll in polls:

		print "Checking %s" %(poll)
		poll_in_srp(srp, poll)


## Set Boolean values for polls
def poll_in_srp(srp, poll):

	# ensure that poll is a FederalPoll object
	assert(isinstance(poll, FederalPoll))
	assert(isinstance(srp, FederalRiding))

	# check for intersection between poll and riding
	if poll.geom.intersects(srp.geom):

		poll.in_riding = True

		# check if poll is completely contained within SRP
		if poll.geom.within(srp.geom):

			poll.srp_overlap = 'F'

		else:

			poll.srp_overlap = 'P'

	# no overlap
	else:

		poll.in_riding = False

		poll.srp_overlap = 'N'


	# save poll
	poll.save()

## Set values indicating whether address points are within the riding
def load_srp_addresspoints():

	srp = FederalRiding.objects.filter(english_name="Scarborough--Rouge Park")[0]

	addresspoints = TorontoAddressPoint.objects.all()

	for addresspoint in addresspoints:

		print "Checking %s" %(addresspoint)
		addresspoint_in_srp(srp, addresspoint)


## Set Boolean values for addresspoints
def addresspoint_in_srp(srp, addresspoint):

	# ensure that address point is a TorontoAddressPoint object
	assert(isinstance(addresspoint, TorontoAddressPoint))
	assert(isinstance(srp, FederalRiding))

	# check for intersection between addresspoint and riding
	if addresspoint.geom.intersects(srp.geom):

		addresspoint.in_riding = True

		# check if addresspoint is completely contained within SRP
		if addresspoint.geom.within(srp.geom):

			addresspoint.srp_overlap = 'F'

		else:

			addresspoint.srp_overlap = 'P'

	# no overlap
	else:

		addresspoint.in_riding = False

		addresspoint.srp_overlap = 'N'


	# save addresspoint
	addresspoint.save()


def load_postalcodefsa(verbose=True):

	# set data directory depending on whether we're in the dev or production environment
	if os.environ.has_key('OPENSHIFT_DATA_DIR'):
		data_directory = os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'geodata')
	else:
		data_directory= os.path.join(os.environ['ELECTION2015_REPO_DIR'], 'data', 'geodata')

	filename = 'CANfsa/SRPfsas.shp'
	shapefile = os.path.join(data_directory, filename)

	lm = LayerMapping(PostalCodeForwardSortationArea, shapefile, postalcodefsa_mapping, transform=True, encoding='utf8')
	lm.save(strict=True, verbose=verbose)


def prune_postalcodefsa():

	outside_fsas = PostalCodeForwardSortationArea.objects.exclude(fsa__in=['M1B', 'M1C', 'M1E', 'M1X'])
	outside_fsas.delete()


def load_postalcodemep(verbose=True):

	# set data directory depending on whether we're in the dev or production environment
	if os.environ.has_key('OPENSHIFT_DATA_DIR'):
		data_directory = os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'geodata')
	else:
		data_directory= os.path.join(os.environ['ELECTION2015_REPO_DIR'], 'data', 'geodata')

	filename = 'CANmep/SRPpostalcodes.shp'
	shapefile = os.path.join(data_directory, filename)

	lm = LayerMapping(PostalCode, shapefile, postalcodemep_mapping, transform=True, encoding='utf8')
	lm.save(strict=True, verbose=verbose)


def prune_postalcode():

	outside_meps = PostalCode.objects.exclude(postalcode__regex=r'^M1(B|C|E|X)')
	outside_meps.delete()


def load_postalcodeforeignkeys():

    postalcodes = PostalCode.objects.all()
    srp = FederalRiding.objects.filter(english_name="Scarborough--Rouge Park")[0]

    for pc in postalcodes:

        print "Checking %s" %(pc)
        process_postalcode(pc, srp)


def process_postalcode(pc, srp):

    # ensure that postal code is a PostalCode object
    assert(isinstance(pc, PostalCode))
    assert(isinstance(srp, FederalRiding))

    pc.fsa = PostalCodeForwardSortationArea(fsa=pc.postalcode[:3].upper())
    pc.riding = FederalRiding.objects.exclude(english_name="Scarborough--Rouge Park").get(geom__contains=pc.geom)
    pc.poll = FederalPoll.objects.get(geom__contains=pc.geom)

    # check for intersection between postal code and Scarborough--Rouge Park
    if pc.geom.intersects(srp.geom):

        pc.in_riding = True

        # check if postal code is completely contained within SRP
        if pc.geom.within(srp.geom):

            pc.srp_overlap = 'F'

        else:

            pc.srp_overlap = 'P'

    # no overlap
    else:

        pc.in_riding = False

        pc.srp_overlap = 'N'

    # save postal code
    pc.save()


# load all ridings from shapefile into database
def load_data(verbose=True):

	# load all ridings
	load_ridings(verbose)
	prune_ridings()

	# load Scarborough Rouge Park geopoints
	load_srpgeopoints()

	# load and prune polls
	load_polls(verbose)
	prune_polls()
	load_pollforeignkeys()

	# load and prune address points
	load_addresspoints()
	prune_addresspoints()
	load_addresspointforeignkeys()
