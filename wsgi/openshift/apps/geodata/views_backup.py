from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist

from django.utils import simplejson

from apps.geodata.models import *



def postal_test(request):

    return render_to_response('geodata/lookup.html', context_instance=RequestContext(request))


def postal(request):

    return render_to_response('geodata/postalcode.html', context_instance=RequestContext(request))


def postal_json(request):

    if request.POST.has_key('postalcode'):

        # postal code object (trimmed, eliminated of spaces and in upper case)
        q_postalcode = request.POST['postalcode']

        # response dictionary
        response_dict = {}

        try:

            postalcodes = PostalCode.objects.filter(postalcode=q_postalcode)

            # make postal code more readable, for JSON object
            q_postalcode = q_postalcode[:3] + " " + q_postalcode[3:]

            pc_in_riding = sum([1 if pc.in_riding else 0 for pc in postalcodes])
            num_pc = len(postalcodes)

            # case 1: all postal codes are in riding
            if pc_in_riding == num_pc:

                postalcode = postalcodes[0]

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': True,
                        'in_riding': True,
                        'srp_overlap': "Full",

                        'fsa': postalcode.fsa,
                        'riding': postalcode.riding.english_name
                    }
                )

            # case 2: some postal codes are in riding
            elif  0 < pc_in_riding < num_pc:

                postalcode = postalcodes[0]

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': True,
                        'in_riding': True,
                        'srp_overlap': "Partial",

                        'fsa': postalcode.fsa,
                        'riding': postalcode.riding.english_name
                    }
                )

            # case 3: no postal codes are in riding, but they can order forms
            else:

                postalcode = postalcodes[0]

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': True,
                        'in_riding': False,
                        'srp_overlap': "None",

                        'fsa': postalcode.fsa,
                        'riding': postalcode.riding.english_name
                    }
                )

        except:

                postalcode = None

                response_dict.update(
                    {
                        'postalcode': q_postalcode,

                        'found': False,
                        'in_riding': False,
                        'srp_overlap': "None",
                    }
                )    

        return HttpResponse(simplejson.dumps(response_dict), mimetype='application/javascript')

    else:

        return render_to_response('geodata/postalcode.html', context_instance=RequestContext(request))
